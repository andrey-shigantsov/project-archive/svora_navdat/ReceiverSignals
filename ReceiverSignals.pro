TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS = InputDriver \
          NAVDAT

InputDriver.file = $$PWD/InputDriver/ReceiverSignals-InputDriver.pro
NAVDAT.file = $$PWD/NAVDAT/ReceiverSignals-NAVDAT.pro

NAVDAT.depends = InputDriver
